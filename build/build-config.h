#ifndef BUILD_CONFIG_H
#define BUILD_CONFIG_H

/* Define to the full name of this package. */
#define PACKAGE_NAME "gromit-mpx"

/* Define to the home page for this package. */
#define PACKAGE_URL "https://gitlab.com/0EFB6/gromit-mpx-willy"

/* Define to the version of this package. */
#define PACKAGE_VERSION "1.5"

/* The package's locale domain. */
#define PACKAGE_LOCALE_DOMAIN "gromit-mpx"

/* The directory where the package's translations reside. */
#define PACKAGE_LOCALE_DIR "/usr/local/share/locale"

#define SYSCONFDIR "/usr/local/etc"

/* This is defined when libappindicator is not libayatana-libappindicator. */
/* #undef APPINDICATOR_IS_LEGACY */

#endif /* BUILD_CONFIG_H */
